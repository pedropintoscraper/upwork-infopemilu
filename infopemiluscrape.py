import requests
import json
from requests import ConnectTimeout
import csv
from multiprocessing import Manager
import multiprocessing
import random
import lxml.html
import time

def scrapeProxies():

	proxiesIps = []

	s = requests.Session()
	link = 'http://www.gatherproxy.com/proxylist/country/?c=Indonesia'
	ipshttp = []
	head = {
		'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
		'Upgrade-Insecure-Requests':'1',
		'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3278.0 Safari/537.36 OPR/51.0.2809.0 (Edition developer)'
	}

	r = s.get(url=link, headers=head)

	print('scraping indonesian proxies... this should take 5 minutes')

	for num in range(1,50):

		#print(num)

		payload = {
			'Country' : 'indonesia',
			'PageIdx' : str(num)
		}

		r = s.post(url = link, headers = head, data=payload)

		tree = lxml.html.fromstring(r.content)

		if tree.xpath('//table[@id="tblproxy"]')[0].xpath('./tr')[2:] == []:
			break

		for i in tree.xpath('//table[@id="tblproxy"]')[0].xpath('./tr')[2:]:

			proxiesIps.append(
				i.xpath('./td')[1].xpath('./script')[0].text.split("'")[1]+':'+str(int(i.xpath('./td')[2].xpath('./script')[0].text.split("'")[1],16))
				)

	return proxiesIps


def testProxies(i):


	head = { 
		'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/45.0',
		'Upgrade-Insecure-Requests':'1'
	}


	proxies = {'https': 'https://'+i}

	link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/JAWA%20BARAT/SUKABUMI/SUKARAJA/SELAWANGI/listDps.json'

	start_time = time.time()
	
	try:
		r = requests.get(url = link, headers=head, proxies=proxies, timeout = 15)
		if r.text.find('aaData') != -1 and r.status_code == 200:

			link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/BANTEN/KOTA TANGERANG/BATUCEPER/BATUCEPER/listDps.json'
			

			r = requests.get(url = link, headers=head, proxies=proxies, timeout = 15)
			if r.text.find('aaData') != -1 and r.status_code == 200:
				if int((time.time() - start_time) < 35):
					#print(str((time.time() - start_time))+ '   :' + i)
					ips.append(i)
	except:
		pass

def scrapeCities():

	city = []

	head = {
		'accept':'application/json, text/javascript, */*; q=0.01',
		'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3230.0 Safari/537.36 OPR/50.0.2753.0 (Edition developer)'
	}

	link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/listNasional.json'

	while(True):
		proxies = {'https': 'https://'+random.choice(ips)}
		try:
			r = requests.get(url=link, headers=head, proxies=proxies, timeout = 20)
			if r.text.find('aaData') != -1 and r.status_code == 200:
				break
		except:
			pass

	for scrapeCity in json.loads(r.text)['aaData']:

		city.append(scrapeCity['namaWilayah'])

	return city

def scrapeProvinces(city):

	head = {
		'accept':'application/json, text/javascript, */*; q=0.01',
		'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3230.0 Safari/537.36 OPR/50.0.2753.0 (Edition developer)'
	}

	link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/'+city+'/listDps.json'

	while(True):
		proxies = {'https': 'https://'+random.choice(ips)}
		try:
			r = requests.get(url=link, headers=head, proxies=proxies, timeout = 20)
			if r.text.find('aaData') != -1 and r.status_code == 200:
				break
		except:
			pass

	for scrapeProvince in json.loads(r.text)['aaData']:

		provinces.append([
			city,
			scrapeProvince['namaKabKota']
			])

	counter.append('.')
	if len(counter)%50 == 0:
		print(len(counter))

def scrapeDistrict(province):

	head = {
		'accept':'application/json, text/javascript, */*; q=0.01',
		'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3230.0 Safari/537.36 OPR/50.0.2753.0 (Edition developer)'
	}

	link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/'+province[0]+'/'+province[1]+'/'+'listDps.json'

	while(True):
		proxies = {'https': 'https://'+random.choice(ips)}
		try:
			r = requests.get(url=link, headers=head, proxies=proxies, timeout = 20)
			if r.text.find('aaData') != -1 and r.status_code == 200:
				break
		except:
			pass

	for scrapeDitrict in json.loads(r.text)['aaData']:

		districts.append([
			province[0],
			province[1],
			scrapeDitrict['namaKecamatan']
			])

	counter.append('.')
	if len(counter)%50 == 0:
		print(len(counter))

def scrapeSubDistrict(district):

	head = {
		'accept':'application/json, text/javascript, */*; q=0.01',
		'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3230.0 Safari/537.36 OPR/50.0.2753.0 (Edition developer)'
	}

	link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/'+district[0]+'/'+district[1]+'/'+district[2].replace('/','~')+'/'+'listDps.json'

	while(True):
		proxies = {'https': 'https://'+random.choice(ips)}
		try:
			r = requests.get(url=link, headers=head, proxies=proxies, timeout = 10)
			if r.text.find('aaData') != -1 and r.status_code == 200:
				break
		except:
			pass

	if len(json.loads(r.text)['aaData']) > 100:
		print(link)

	for scrapeSuDistrict in json.loads(r.text)['aaData']:

		subdistricts.append([
			district[0],
			district[1],
			district[2],
			scrapeSuDistrict['namaKelurahan']
			])
	
	counter.append('.')
	if len(counter)%50 == 0:
		print(len(counter))

def scrapeStations(subdistrict):

	head = {
		'accept':'application/json, text/javascript, */*; q=0.01',
		'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3230.0 Safari/537.36 OPR/50.0.2753.0 (Edition developer)'
	}

	link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/'+subdistrict[0]+'/'+subdistrict[1]+'/'+subdistrict[2].replace('/','~')+'/'+subdistrict[3].replace('/','~')+'/'+'listDps.json'

	while(True):
		proxies = {'https': 'https://'+random.choice(ips)}
		try:
			r = requests.get(url=link, headers=head, proxies=proxies, timeout = 20)
			if r.text.find('Server sedang') != -1:
				stations.append([
					subdistrict[0],
					subdistrict[1],
					subdistrict[2],
					subdistrict[3],
					'0'
					])					
				return 0
			if r.text.find('aaData') != -1:
				break
		except:
			pass

	for scrapeStation in json.loads(r.text)['aaData']:

		stations.append([
			subdistrict[0],
			subdistrict[1],
			subdistrict[2],
			subdistrict[3],
			scrapeStation['tps']
			])	

	counter.append('.')
	if len(counter)%5 == 0:
		print(len(counter))

def scrapevoters(station):

	head = {
		'accept':'application/json, text/javascript, */*; q=0.01',
		'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3230.0 Safari/537.36 OPR/50.0.2753.0 (Edition developer)'
	}

	link = 'https://infopemilu.kpu.go.id/pilkada2018/pemilih/dpt/1/'+station[0]+'/'+station[1]+'/'+station[2].replace('/','~')+'/'+station[3].replace('/','~')+'/'+station[4]+'/'+'listDps.json'
	
	while(True):
		proxies = {'https': 'https://'+random.choice(ips)}
		try:
			'''
			Scraping stations with boots, 0 means didn't scrape
			'''
			if station[-1] != '0':

				r = requests.get(url=link, headers=head, proxies=proxies, timeout = 20)
				if r.text.find('Server sedang') != -1:
					#print(link + '-----> BUSY')
					failedlinks.append([link])
					return 0
				if r.text.find('aaData') != -1:
					break
		except:
			pass

	for scrapeVoter in json.loads(r.text)['aaData']:

		name = ''
		nik = ''
		tempp = ''
		jenis = ''

		try:
			name = scrapeVoter['nama']
		except:
			pass

		try:
			nik = scrapeVoter['nik']
		except:
			pass

		try:
			tempp = scrapeVoter['tempatLahir']
		except:
			pass

		try:
			jenis = scrapeVoter['jenisKelamin']
		except:
			pass

		voters.append([
			station[0],
			station[1],
			station[2],
			station[3],
			station[4],
			name,
			nik,
			tempp,
			jenis
			])

	counter.append('.')
	if len(counter)%5 == 0:
		print(str(len(counter)) + ' - ' + str(len(voters)))




##############################################################################################################

directory = '/home/pedro/'

ipshttp = scrapeProxies()

manager = Manager()
ips = manager.list([])

pool = multiprocessing.Pool(processes=200)
pool.map(testProxies,ipshttp)
pool.close()
pool.join()

print('scrape cities')
cities = scrapeCities()

print('scrape provinces')
provinces = manager.list([])
counter = manager.list([])

pool = multiprocessing.Pool(processes=200)
pool.map(scrapeProvinces,cities)
pool.close()
pool.join()

print('scrape districts')
districts = manager.list([])
counter = manager.list([])

pool = multiprocessing.Pool(processes=200)
pool.map(scrapeDistrict,provinces)
pool.close()
pool.join()


#refresh ip pool list


ips = manager.list([])
ipshttp = scrapeProxies()
pool = multiprocessing.Pool(processes=200)
pool.map(testProxies,ipshttp)
pool.close()
pool.join()



print('scrape sub districts')
subdistricts = manager.list([])
counter = manager.list([])

pool = multiprocessing.Pool(processes=200)
pool.map(scrapeSubDistrict,districts)
pool.close()
pool.join()


#refresh ip pool list


ips = manager.list([])
ipshttp = scrapeProxies()
pool = multiprocessing.Pool(processes=200)
pool.map(testProxies,ipshttp)
pool.close()
pool.join()


print('scrape stations')
stations = manager.list([])
counter = manager.list([])

pool = multiprocessing.Pool(processes=200)
pool.map(scrapeStations,subdistricts)
pool.close()
pool.join()

#Sample scraping voters of Bali

baliscrape = []

for i in stations:
	if i[0] == 'BALI':
		baliscrape.append(i)

print('Scrape Bali voters')
voters = manager.list([])
counter = manager.list([])

ips = manager.list([])

pool = multiprocessing.Pool(processes=200)
pool.map(testProxies,ipshttp)
pool.close()
pool.join()

pool = multiprocessing.Pool(processes=200)
pool.map(scrapevoters,baliscrape)
pool.close()
pool.join()

outputfile = open(directory+'infopermiluvoters_bali.csv','w',newline='')
outputwriter = csv.writer(outputfile)
for i in voters:
    outputwriter.writerow(i)
outputfile.close()